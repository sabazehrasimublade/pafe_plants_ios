//
//  ContentView.swift
//  PafePlants
//
//  Created by Mac on 16/11/2020.
//

import SwiftUI

struct ContentView: View {
    @State var isActive : Bool = false
    var body: some View {
        
        ZStack{
            Image("backgroundImage").resizable()
                .aspectRatio(contentMode: .fill).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)

            VStack(alignment : .leading){
                Image("pafeLogo")
                Divider()
                Text("it's all").font(Font.custom("JosefinSans-SemiBold", size: 33 )).foregroundColor(Color (AppColor.green)).frame(alignment : .leading)
                Text("about Plants").font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(Color (AppColor.green))
                Divider()
                Text("Praesent sollicitudin leo et urna luctus , sed dictum neque ornare.").font(Font.custom("JosefinSans-Regular" , size: 17)).foregroundColor(Color (AppColor.lightshed))
                Divider()
                Image("wimg").aspectRatio(contentMode: .fit)
                Divider()
                NavigationLink(destination: signin().navigationBarTitle("next").navigationBarHidden(true), isActive: self.$isActive){
                Button(action: {
                    print("button Tapped")
                    self.isActive = true
                    self.reactOnButtonClick()
                }) {
                    Image("arrow")

                }.background(Color(AppColor.lightshed)).frame(width: 80, height: 50).cornerRadius(5)

                }


            }.padding(40)


            
//            Text("pafe plants")
       }
        
       
    }
    
    private func reactOnButtonClick() {
          NavigationLink(
            destination: signin(),
            label: {
                Text("")
            })
        }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}




                            
                 
 
