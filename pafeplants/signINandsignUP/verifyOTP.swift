//
//  verifyOTP.swift
//  PafePlants
//
//  Created by Mac on 18/11/2020.
//

import SwiftUI

struct verifyOTP: View {
    
    @State var CountryCode : String = "+1"
    @State var PhoneNumber : String = ""
    var body: some View {
       
        ZStack{
            Image("backgroundImage").resizable()
                .aspectRatio(contentMode: .fill).edgesIgnoringSafeArea(.all)
            
            VStack(alignment : .leading){
                Image("pafeLogo")
                Divider()
                Text("Enter").font(Font.custom("JosefinSans-SemiBold", size: 33 )).foregroundColor(Color (AppColor.green)).frame(alignment : .leading)
                Text("Verification code.").font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(Color (AppColor.green))
                Divider()
                Text("OTP sent on your mobile phone.").font(Font.custom("JosefinSans-Regular" , size: 17)).foregroundColor(Color (AppColor.lightshed))
                Divider()
                HStack{
                    TextField("+1", text: $CountryCode).font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(Color(AppColor.lightshed))
                    Divider()
                    TextField("000000000000", text: $PhoneNumber).font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(.white)
                    
                }.padding().frame(height : 80)
                Text("2:02").foregroundColor(Color(AppColor.green)).font(Font.custom("JosefinSans-SemiBold" , size: 17))
                
                Spacer()
                ZStack{
                RoundedRectangle(cornerRadius: 30)
                Button(action: {print("button next Tapped")}) {
                    Text("NEXT").font(Font.custom("JosefinSans-SemiBold" , size: 17)).foregroundColor((Color(AppColor.appTheme)))
                    
                }.background(Color(AppColor.green)).frame()
                
                }.frame(height : 60).foregroundColor(Color(AppColor.green)).cornerRadius(30).padding()
                
                
            }.padding()
        }
    }
}

struct verifyOTP_Previews: PreviewProvider {
    static var previews: some View {
        verifyOTP()
    }
}
