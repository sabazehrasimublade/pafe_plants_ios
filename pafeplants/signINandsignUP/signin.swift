//
//  signin.swift
//  PafePlants
//
//  Created by Mac on 18/11/2020.
//

import SwiftUI


struct signin: View {
    
    @State var CountryCode : String = "+1"
    @State var PhoneNumber : String = ""
    var body: some View {
       
        ZStack{
            Image("backgroundImage").resizable()
                .aspectRatio(contentMode: .fill).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            
            VStack(alignment : .leading){
                Image("pafeLogo")
                Divider()
                Text("Enter your").font(Font.custom("JosefinSans-SemiBold", size: 33 )).foregroundColor(Color (AppColor.green)).frame(alignment : .leading)
                Text("mobile number.").font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(Color (AppColor.green))
                Divider()
                Text("We will send you a security code.").font(Font.custom("JosefinSans-Regular" , size: 17)).foregroundColor(Color (AppColor.lightshed))
                Divider()
                HStack{
                    TextField("+1", text: $CountryCode).font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(Color(AppColor.lightshed))
                    Divider()
                    TextField("000000000000", text: $PhoneNumber).font(Font.custom("JosefinSans-SemiBold" , size: 33)).foregroundColor(.white)
                    
                }.padding().frame(height : 80)
                Spacer()
                ZStack{
                RoundedRectangle(cornerRadius: 30)
                Button(action: {print("button next Tapped")}) {
                    Text("NEXT").font(Font.custom("JosefinSans-SemiBold" , size: 17)).foregroundColor((Color(AppColor.appTheme)))
                    
                }.background(Color(AppColor.green)).frame()
                
                }.frame(height : 60).foregroundColor(Color(AppColor.green)).cornerRadius(30).padding()
                
                
            }.padding(30)
        }
    }
}

struct signin_Previews: PreviewProvider {
    static var previews: some View {
        signin()
    }
}
