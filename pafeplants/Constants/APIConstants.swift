//
//  APIConstants.swift

//
//  Created by DAY on 13/04/20.
//  Copyright © 2020 DAY. All rights reserved.
//

import Foundation

let baseUrl = "https://pvcz1rzmnc.execute-api.us-east-2.amazonaws.com/default/"

let kCreateUser = "user/createUser"
let kGetEntity = "entity/getEntity"
let kGetUser = "user/getUser"
let kGetCheckinId = "log/getCheckedIn"
let kLogCheckinLog = "log/checkInLog"
let kSubmitSymptom = "user/submitSurvey"
let kGetLogs  = "log/getLogs"
let kGetExposureLogs  = "exposure/getExposureLogs"
let kCheckOut = "log/checkoutPreviousLog"
let kGetStatus = "user/getStatus"
let kGetRoomList = "room/listRooms"
let kActiveCaseStatus = "case/getActiveCaseStatus"






