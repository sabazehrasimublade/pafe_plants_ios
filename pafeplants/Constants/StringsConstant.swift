//
//  StringsConstant.swift

//
//  Created by DAY on 15/04/20.
//  Copyright © 2020 DAY. All rights reserved.
//

import Foundation



//MARK:-  Buttons
struct ButtonText {
    
    static let forgotPassword = "Forgot Password?"
    static let get_started = "GET STARTED"
    static let skip_all = "SKIP ALL"
    static let skip = "SKIP"
    static let i_have_an_account = "I HAVE AN ACCOUNT"
    static let go_back = "GO BACK"
    static let maybe_later = "MAYBE LATER"
    static let _continue = "CONTINUE"
    static let save_settings = "SAVE SETTINGS"
    static let already_have_an_account = "ALREADY HAVE AN ACCOUNT"
    static let log_in = "LOG IN"
    static let try_again = "Try Again"
    static let ok = "OK"
    static let cancel = "CANCEL"
    static let resend = "RESEND"
    static let withdarw = "WITHDRAW"
    static let deposit = "DEPOSIT"
    static let Withdarw = "Withdrawals"
    static let withdraw = "Withdraw"
    static let Deposit = "Deposit"
    static let Go_back = "Go Back"
    static let go_to_profile = "Go To Profile"
    static let buy = "BUY"
    static let obsipay = "OBSIPAY"
    static let view_all = "View all"
    static let verified = "Verified"
    static let get_verified = "Get Verified"
    
    static let send_coin = "Send Coins"
    static let add_user = "Add User"
    static let friends =  "Friends"
    static let recentTransfer = "Recent Transfers"
    
    
    static let pleaseWait = "PLEASE WAIT..."
    static let changeEmail = "Change email"
    static let updatePhone = "Update Phone"
    static let sendSMSCodeAgain = "Send SMS Code Again"
    static let paste = "Paste"
    static let changePhone = "Change Phone"
    static let sendSMSCode = "Send SMS Code"
    static let switchToDifferentAccount = "Switch to different account"
    
    static let send = "SEND"
    static let next = "NEXT"
    static let change_to = "Change to"
    static let selected_currency = "SELECTED CURRENCY"
    static let selected_coin = "SELECTED COIN"
    static let send_to = "SEND TO"
    
    static let log_out = "Log Out"
    static let unlock = "Unlock"
}


struct ValidationMessage {
    static let scan_valid = "This is not a valid QR Code"
     static let qr_scan_failed = "QR Code scanning failed"
    static let blankUsername = "Please enter Full Name."
    static let blankEmployId = "Please enter Employ ID."
    static let minLengthUsername = "Username must have more than 2 characters."
    static let invalidUserName = "Username can't contain special characters except !@#$%^&_."
    static let blankEmail = "Please enter an email address."
    static let invalidEmail = "Please enter a valid email address."
    static let blankPhoneNumber = "Please enter phone number."
    static let minLengthPhoneNumber = "Phone number must be greater than 7 and less than 16 digit."
    static let invalidPhoneNumber = "Please enter valid phone number."
    static let blankExperienceLevel = "Please select experience level"
    static let blankReferralCode = "Please enter referral code"
    static let invalidReferralCode = "Please enter valid referral code"
    
    
    
    static let please_enter_amount_greater_than = "Please enter an amount greater than"
    static let plase_enter_amount_lesser_than_available_balance = "Please enter an amount lesser than your available balance."
    static let your_transfer_daily_limit_reached = "Your transfer daily limit reached.";
    
    
    // Login
    static let validUsername_email = "Enter username or email."
    static let validUserName = "Enter valid username."
    static let validEmail = "Enter valid email."
    static let minPassword = "Password must be 8 characters long."
    static let alphanemericPassword = "Password must be 8-16 characters long, must have at least 1 lowercase, 1 uppercase, 1 number & a special character eg. !@#$%^&."
    static let alphanemericPasswordWithCap = "Must have at least 1 lowercase, 1 uppercase, 1 number & a special character."
    static let minPhoneNumber = "Phone number must be 8 characters long."
    static let validPhone  =  "Please enter valid phone number."
    
    
    
    //Password
    static let minLengthPassword = "Password must greater than 8 and less than 16 characters."
    static let blankNewPassword = "Please enter new password."
    static let invalidNewPassword = "New Password must be 8-16 characters long, must have at least 1 lowercase, 1 uppercase, 1 number & a special character eg. !@#$%^&."
    static let blankPassword = "Please enter password."
    static let invalidPassword = "Password must be 8-16 characters long, must have at least 1 lowercase, 1 uppercase, 1 number & a special character eg. !@#$%^&."
    static let passwordNotMatch = "The passwords do not match."
    static let passwordMustHave = "Must have A-Z, a-z, 0-9 & a special character."
}

