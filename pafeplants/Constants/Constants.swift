//
//  Constants.swift
//  PafePlants
//
//  Created by Mac on 18/11/2020.
//

import Foundation
import UIKit



//MARK:- Global Variables
var AppName: String {return Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String}
var AppDisplayName: String {return Bundle.main.infoDictionary!["CFBundleName"] as! String}
var AppVersion: String {return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String}
var AppBuildVersion: String {return Bundle.main.infoDictionary!["CFBundleVersion"] as! String}

//MARK:- UIDevice Static Constants
let UserDefault     = UserDefaults.standard


let IS_IPHONE_5             = 568.0
let IPHONE6_PLUS_WIDTH      = 414.0
let IPHONE6_PLUS_HEIGHT     = 736.0
let IPHONE6_WIDTH           = 375.0
let IPHONE6_HEIGHT          = 667.0
let IPHONEX_HEIGHT          = 812.0

let isIpad = UIDevice.current.userInterfaceIdiom == .pad
let Screen = UIScreen.main.bounds.size
let kWindowWidth =  Screen.width
let kWindowHeight =  Screen.height
let charSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")



public func delay(bySeconds seconds: Double, dispatchLevel: DispatchLevel = .main, closure: @escaping () -> Void) {
    let dispatchTime = DispatchTime.now() + seconds
    dispatchLevel.dispatchQueue.asyncAfter(deadline: dispatchTime, execute: closure)
}

public enum DispatchLevel {
    case main, userInteractive, userInitiated, utility, background
    var dispatchQueue: DispatchQueue {
        switch self {
        case .main:                 return DispatchQueue.main
        case .userInteractive:      return DispatchQueue.global(qos: .userInteractive)
        case .userInitiated:        return DispatchQueue.global(qos: .userInitiated)
        case .utility:              return DispatchQueue.global(qos: .utility)
        case .background:           return DispatchQueue.global(qos: .background)
        }
    }
}


let emailMaxLength                  =       80
let firstNameMaxLength              =       32
let lastNameMaxLength               =       32
let nameMaxLength                   =       64
let OTPLength                       =       6
let passwordMaxLength               =       16
let mobileNoMaxLength               =       15
let refferalCodeMaxLength           =       32




//MARK:- Fonts
enum PlayfairDisplay : String {
    
   
    case Bold = "PlayfairDisplay-Bold"
    case Regular = "PlayfairDisplay-Regular"
    
    static func getFont(_ fontType: PlayfairDisplay, size: CGFloat) -> UIFont {
        return UIFont(name: fontType.rawValue, size: size) ?? UIFont.systemFont(ofSize: size, weight: .regular)
    }
    
}

enum JosefinSans: String {
    
    case Regular = "JosefinSans-Regular"
    case Bold = "JosefinSans-Bold"
    case Light = "JosefinSans-Light"
    case semibold = "JosefinSans-SemiBold"

    static func getFont(_ fontType: JosefinSans, size: CGFloat) -> UIFont {
        return UIFont(name: fontType.rawValue, size: size) ?? UIFont.systemFont(ofSize: size, weight: .regular)
    }
    
}


struct UserDefaultsKey {
    static let currencyList = "currency-list"
}





//MARK:- Colors

struct AppColor {
    
    static let rgb = { (red: CGFloat,green:CGFloat,blue:CGFloat,alpha:CGFloat) -> UIColor in
        return UIColor(red:red/255.0, green:green/255.0, blue:blue/255.0, alpha:alpha)
    }

    static let GreyShade = AppColor.rgb(123,117,111,1)
    static let appTheme = #colorLiteral(red: 0.1607843137, green: 0.1176470588, blue: 0.07450980392, alpha: 1)
    static let lightshed = #colorLiteral(red: 0.9764705882, green: 0.9843137255, blue: 0.9882352941, alpha: 1)
    static let green = #colorLiteral(red: 0.06666666667, green: 0.8509803922, blue: 0.2078431373, alpha: 1)
   
    
   

}

public func isDarkMode() -> Bool {
    if #available(iOS 13.0, *) {
        if UITraitCollection.current.userInterfaceStyle == .dark {
            return true
        }
        else {
            return false
        }
    }
    return false
}

